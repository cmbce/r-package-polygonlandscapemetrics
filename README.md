# README #

This package allows to compute metrics of composition (area) and distribution (linearity, perimeters) around given points based on vector spatial data.

### What is this repository for? ###

* To compute landscape metrics all over a landscape of a limited size, analysis of raster data with softs like [FragStats](http://www.umass.edu/landeco/research/fragstats/documents/Metrics/Metrics%20TOC.htm) or 
[Chloe](https://www6.rennes.inra.fr/sad/Outils-Produits/Outils-informatiques/Chloe), but when you want to assign such metrics to points sparsed on a large territory such as France, the data is big enough 
that you need to work with shape format, it is the purpose of this package. 
* This is currently under active development but already in production to compute composition metrics within 10km of 10000 of points all over points based on databases 
[see article in french](https://ecosys.versailles-grignon.inra.fr/SpatialAgronomy/publications/Barbu2017.pdf)

### How do I get set up? ###

* Install devtools R package

* Automated install with devtools :
``` R 
	devtools::install_git("https://bitbucket.org/cmbce/r-package-datamanagement.git",subdir="DataManagement", build_manual=TRUE)
	devtools::install_git("https://bitbucket.org/cmbce/r-package-polygonlandscapemetrics.git",subdir="polygonLandscapeMetrics", build_manual=TRUE)
``` 

* Or, manual install :

	* Install [DataManagement](https://bitbucket.org/cmbce/r-package-datamanagement.git)
	* Dowload/clone this repo
	* open R in the polygonLandscapeMetrics folder, then in R: 
``` R
    library("devtools")
	document()
	install()
``` 
	
* You can then use 
``` R
    library("polygonLandscapeMetrics")
	
```
### Contribution guidelines ###

* Contributions are welcome, please use the pull request system. 

### Who do I talk to? ###

* For suggestions and issues please use the issue tracker. 