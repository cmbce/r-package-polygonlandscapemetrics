#' Boundaries of woods in remote French location
#' @description Polygon delimiting woods in remote French countryside in the Eure departement
#' @docType data
#' @keywords datasets
#' @name woods
#' @usage data(woods)
#' @format A SpatialPolygonsDataFrame object.
NULL

#' Points near or in the polygons defined in \code{woods}
#' @description A SpatialPoints object with points in or near the polygons defined in \code{woods}
#' @docType data
#' @keywords datasets
#' @name centers
#' @usage data(centers)
#' @details
#' It is defined as: \cr
#' centers <- data.frame(lon=c(1.395,1.394,1.3930,1.388), \cr
#'                       lat=c(48.971,48.973,48.9729,48.966)) \cr
#' coordinates(centers) <- ~lon+lat \cr
#' proj4string(centers) <- proj4string(woods) \cr
#' @format A SpatialPoints object.
NULL

#' Nested Squares
#' @description A SpatialPolygons object with two nested squares in euclidian space
#' @docType data
#' @keywords datasets
#' @name nestedSquares
#' @usage data(nestedSquares)
#' @details
#' They are defined as: \cr
#' nestedSquares <- SpatialPolygons(list(Polygons(list(Polygon(cbind(c(0,1,1,0,0),
#'                                                                   c(0,0,1,1,0)),
#'                                                             hole=FALSE)),1),\cr
#'                                       Polygons(list(Polygon(cbind(c(-1,2,2,-1,-1),
#'                                                                   c(-1,-1,2,2,-1)),
#'                                                             hole=FALSE)),2)))
#' @format A SpatialPolygons object.
NULL

#' Joint Squares
#' @description A SpatialPolygons object with two joint squares in the euclidian space
#' @docType data
#' @keywords datasets
#' @name jointSquares
#' @usage data(jointSquares)
#' @details
#' They are defined as: \cr
#' jointSquares <- SpatialPolygons(list(Polygons(list(Polygon(cbind(c(0,1,1,0,0),
#'                                                                  c(0,0,1,1,0)),hole=FALSE)),1), \cr
#'                                               Polygons(list(Polygon(cbind(c(0,1,1,0,0),
#'                                                                           1+c(0,0,1,1,0)),
#'                                                                     hole=FALSE)),3)))
#' @format A SpatialPolygons object.
NULL

#' Diverse geometric forms
#' @description A SpatialPolygons object with diverse forms in the euclidian space
#' @docType data
#' @keywords datasets
#' @name forms
#' @usage data(forms)
#' @details
#' They are defined as: \cr
#' equilateralTriangle <- Polygons(list(Polygon(cbind(c(-0.5,0.5,0,-0.5)+0.5,
#'                                                    c(0,0,sqrt(3/4),0)-0.9),hole=FALSE)),3)
#'          serpent <- Polygons(list(Polygon(cbind(c(-1.5,-1.4,-1.4,1.4,1.4,1.5, 1.5,-1.5,-1.5),
#'                                                 c(  -2,  -2, 1.9,1.9, -2, -2,   2,   2,-2)-0.1),#'                                           hole=FALSE)),4)
#'          spPolygons <- SpatialPolygons(list(equilateralTriangle,
#'                                             serpent))
#'          forms <- rbind(nestedSquares,spPolygons)
#' @format A SpatialPolygons object.
NULL

