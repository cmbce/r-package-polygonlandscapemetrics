library("RPG")
library("devtools")
library("sp")
library("testthat")
library("polygonLandscapeMetrics")
ilots14 <- RPGload("ilots_2012_014")
cult14 <- RPGload("ilotsCult_2012_014")
library("bioagresseurs")
colza14 <- colza[which(colza$codeDepartement== 14),]
colza14 <- ForceProjOfFirst(ilots14,colza14)
#~test<-GetIntersectionBuffersPolygons(colza14,ilots14[a,],bufSize,quadsegs=4)
library("parallel")
library("rgeos")
# parallel doesn't work ok: output is not easy to parallelize
# system.time(test<-ParBal(GetIntersectionBuffersPolygons,sp1=colza14[1:20,],sp2=ilots14[a,],width=bufSize,quadsegs=4,nCores=2))
# system.time(test<-ParBal(GetPolygonMeasuresInBuffer,colza14[1:10,],ilots14[a,],bufSize,quadsegs=4))

bufSize <- 10000
stop()
system.time({
    a<-which(apply(gDistanceByIdPar(colza14[1:10,],ilots14),1,min)<bufSize)
    testRef<-GetPolygonMeasuresInBuffer(colza14[1:10,],ilots14[a,],bufSize,quadsegs=4)
})
#=> 25s

system.time({
    dists <- gDistanceByIdPar(colza14[1:10,],ilots14)
    for(i in 1:10){
        a<-which(dists[,i]<bufSize)
        test<-GetPolygonMeasuresInBuffer(colza14[i,],ilots14[a,],bufSize,quadsegs=4)
    }
})
#=> 20s
#=> clearly faster=> parallellism will rock

system.time({
    nCores <- 8
    nPoints <- 10
    dists <- gDistanceByIdPar(colza14[1:nPoints,],ilots14)
    fn<-function(iS,...){
        cat("iS:",iS,"\n")
        test <- matrix(NA,ncol=3,nrow=length(iS))
        for(i in iS){
            a<-which(dists[,i]<bufSize)
            test<-GetPolygonMeasuresInBuffer(colza14[i,],ilots14[a,],bufSize,quadsegs=4)
        }
    }
    iSplit <- splitIndices(dim(colza14[1:nPoints,])[1],nCores)
    pOut <- mclapply(X=iSplit,FUN=fn,mc.cores=nCores,mc.allow.recursive=FALSE)
})
#=> 7.3 s (/3) not bad
#=> 60s for the ~ all the points => good 

system.time({
    a<-which(apply(gDistanceByIdPar(colza14[1:10,],ilots14),1,min)<bufSize)
    test<-GetPolygonMeasuresInBufferPar(colza14[1:10,],ilots14[a,],bufSize,quadsegs=4)
})
#=> ok similar or shorter in time to the whole loop thingy
expect_almost_equal(testRef,test[,-1],prec=10e-3)
# this is NOT the same for a good reason : the preliminary filter removes things that could, due
# to the imperfectly round buffer (see quadsegs) be partially included
# it is not an issue as the errors are minor
# Note: the difference can be arbitrarily made smaller by increasing quadsegs
